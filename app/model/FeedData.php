<?php

class ModelFeedData extends Model
{
    /**
     * @return string[][]|void
     */
    public function getData()
    {
        // Data emulation
        return array(
            array(
                'period' => '1 час',
                'ci' => '0'
            ),
            array(
                'period' => '6 часов',
                'ci' => '1'
            ),
            array(
                'period' => '12 часов',
                'ci' => '2'
            ),
            array(
                'period' => '24 часа',
                'ci' => '3'
            ),
            array(
                'period' => '2 недели',
                'ci' => '4'
            ),
            array(
                'period' => 'ТЕСТ',
                'ci' => '5'
            ),
        );
    }

    /**
     * @return string[][]|void
     */
    public function getProxyData()
    {
        // Data emulation
        return array(
            array(
                'ip' => '5.164.195.191:8080'
            ),
            array(
                'ip' => '82.119.170.106:8080'
            ),
            array(
                'ip' => '82.102.10.21:3128'
            )
        );
    }
}
