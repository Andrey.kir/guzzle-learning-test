<?php

class HomeLoader extends Controller
{
    /**
     * @return void
     */
    public function actionLoad()
    {
        $this->view->generate('home.php', 'mainTemplate.php');
    }
}
