<?php

class Error404Loader extends Controller
{
    /**
     * @return void
     */
    function actionLoad()
    {
        $this->view->generate('error404.php', 'mainTemplate.php');
    }
}
