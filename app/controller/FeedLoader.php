<?php

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;

class FeedLoader extends Controller
{
    /**
     * @var ModelFeedData
     */
    private $model;
    private $proxyData;

    public function __construct()
    {
        parent::__construct();
        $this->model = new ModelFeedData();
        $this->proxyData = $this->model->getProxyData();
        session_start();
    }

    /**
     * @return void
     */
    public function actionLoad()
    {
        header("Refresh:3600");

        $viewData = $this->getViewData();

        $this->view->generate('feed.php', 'mainTemplate.php', $viewData);
    }

    /**
     * @return string
     */
    private function getSessionId()
    {
        session_regenerate_id();
        return session_id();
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getViewData()
    {
        $data = $this->model->getData();
        $jar = new CookieJar;
        $domain = 'www.bestchange.ru';
        $sessionId = $this->getSessionId();
        $values = ['PHPSESSID' => $sessionId];
        $cookieJar = $jar->fromArray($values, $domain);
        $viewData = [];

        $client = new Client([
            'base_uri' => 'https://www.bestchange.ru',
            'timeout' => 10.0,
            'cookies' => $cookieJar,
        ]);

        foreach ($data as $row) {
            $imageHtml = $this->ipChanger($client, $row, $sessionId, $this->proxyData[0]['ip']);
            array_push($viewData, array('period' => $row['period'], 'img' => $imageHtml));
        }

        return $viewData;
    }

    /**
     * @param Client $client
     * @param array $row
     * @param string $sessionId
     * @param string $proxyIp
     * @param int $ipCount
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function ipChanger(
        Client $client,
        array $row,
        $sessionId,
        $proxyIp,
        $ipCount = 0)
    {
        $params = [
            'proxy' => [
                'https' => $proxyIp,
            ]
        ];

        $ipArrayLimiter  = count($this->proxyData) - 1;

        try {
            $response = $client->request('GET', '/chart.php?type=6&ci=' . $row['ci'] . '&rnd=0.8463267086425017&session=' . $sessionId, $params);
            $body = $response->getBody()->getContents();
            $imageHtml = "<img src='data:image/png;base64," . base64_encode($body) . "'>";
            return $imageHtml;
        } catch (RequestException $e) {
            $ipCount++;
            if ($ipCount >= $ipArrayLimiter ) return 'No data';
            $nextProxyIp = $this->proxyData[$ipCount]['ip'];
            $imageHtml = $this->ipChanger($client, $row, $sessionId, $nextProxyIp, $ipCount);
            return $imageHtml;
        }

    }
}
