<?php

class Route
{
    static public function start()
    {
        $baseControllerName = 'home';
        $actionName = 'load';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if ( !empty($routes[1]) )
        {
            $baseControllerName = $routes[1];
        }

        $controllerName = ucfirst($baseControllerName).'Loader';
        $modelName = ucfirst($baseControllerName) . 'Data';
        $actionName = 'action' . ucfirst($actionName);

        $modelFile = $modelName.'.php';
        $modelPath = "app/model/".$modelFile;

        if(file_exists($modelPath))
        {
            include "app/model/".$modelFile;
        }

        $controllerFile = $controllerName.'.php';
        $controllerPath = "app/controller/".$controllerFile;

        if(file_exists($controllerPath))
        {
            include "app/controller/".$controllerFile;
        }
        else
        {
            Route::page404();
        }

        $controller = new $controllerName;
        $action = $actionName;

        if(method_exists($controller, $action))
        {
            $controller->$action();
        }
        else
        {
            Route::page404();
        }

    }

    /**
     * @return void
     */
    static private function page404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'error404');
    }
}
