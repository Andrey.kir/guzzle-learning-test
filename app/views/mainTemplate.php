<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>TEST</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="menu">
            <ul>
                <li class="first"><a href="/">Главная</a></li>
                <li><a href="/feed">Лента графиков</a></li>
            </ul>
            <br class="clearfix" />
        </div>
    </div>
    <div id="page">
        <div id="content">
            <div class="box">
                <?php include 'app/views/'.$content_view; ?>
            </div>
            <br class="clearfix" />
        </div>
        <br class="clearfix" />
    </div>
    <div id="page-bottom">
        <div id="page-bottom-sidebar">
            <h3>Мои контакты</h3>
            <ul class="list">
                <li class="first"></li>
                <li>skypeid: 8929fcf75aa60ed7</li>
                <li class="last">email: andrey.kir.dev@gmail.com</li>
            </ul>
        </div>
        <div id="page-bottom-content">
            <h3>О Компании</h3>
            <p>
                TEST
            </p>
        </div>
        <br class="clearfix" />
    </div>
</div>
<div id="footer">
    <a href="/">TEST</a> &copy; <?php echo date("Y");?></a>
</div>
</body>
</html>